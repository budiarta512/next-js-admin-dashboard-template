/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    BASE_URL: "/",
    IS_PRODUCTION: "1",
    SERVER_URL: "https://api.test.com",
    LOCAL_API_URL: "http://192.168.0.72:8000/api",
    API_URL: "https://api.test.com/api",
    LOCAL_STORAGE_URL: "http://192.168.0.72:8000/storage/",
    STORAGE_URL: "https://api.test.com/storage/",
  },
};

export default nextConfig;
