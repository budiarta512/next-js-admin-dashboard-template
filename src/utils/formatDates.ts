import moment from "moment";

export const getDateRange = (firstDate: Date, lastDate: Date) => {
  if (
    moment(firstDate, "YYYY-MM-DD").isSame(
      moment(lastDate, "YYYY-MM-DD"),
      "day"
    )
  )
    return [lastDate];
  let date = firstDate;
  const dates = [date];
  do {
    date = moment(date).add(1, "day").toDate();
    dates.push(date);
  } while (moment(date).isBefore(lastDate));
  return dates;
};
