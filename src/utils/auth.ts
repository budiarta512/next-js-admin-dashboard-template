const setToken = (token: string) => {
  localStorage.setItem('token', token);
};
const removeToken = () => {
  localStorage.removeItem('token');
};

const getToken = () => {
  const token = localStorage.getItem('token');
  return token;
};

const isLogin = () => {
  const token = localStorage.getItem('token');
  if (token) {
    return true;
  } else {
    return false;
  }
};

export { setToken, isLogin, removeToken, getToken };
