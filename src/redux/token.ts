class Token {
  getLocalAccessToken() {
    return localStorage.getItem("token");
  }
}

export default new Token();
