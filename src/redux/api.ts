import axios from "axios";
import Token from "./token";
import statusHandler from "./statusHandle";

let baseURL = "";

if (process.env.IS_PRODUCTION === "1") {
  baseURL = process.env.API_URL || "";
} else {
  baseURL = process.env.LOCAL_API_URL || "";
}

const api = axios.create({
  baseURL,
  withCredentials: false,
  headers: {
    "Content-Type": "application/json",
  },
});

// request
api.interceptors.request.use(
  async (config: any) => {
    const token = Token.getLocalAccessToken();
    const token2 = ""; //store.getState().auth.token;
    if (token) {
      config.headers["Authorization"] = "Bearer " + (token2 ? token2 : token);
    }

    return config;
  },
  (error) => {
    console.log("err", error);
    return Promise.reject(error);
  }
);

// response
api.interceptors.response.use(
  function (response) {
    return response;
  },
  async function (error) {
    statusHandler(error);
    return Promise.reject(error);
  }
);

export { api };
