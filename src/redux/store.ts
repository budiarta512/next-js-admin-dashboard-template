import { combineReducers, configureStore } from "@reduxjs/toolkit";
import settingSlice from "./features/settingSlice/settingSlice";

const settingReducerCombi = combineReducers({
  setting: settingSlice,
});

export const makeStore = () => {
  return configureStore({
    reducer: {
      setting: settingReducerCombi,
    },
    // middleware: (getDefaultMiddleware) =>
    //   getDefaultMiddleware({
    //     serializableCheck: false,
    //   }),
    // devTools: true,
  });
}

// Infer the type of makeStore
export type AppStore = ReturnType<typeof makeStore>;
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<AppStore["getState"]>;
export type AppDispatch = AppStore["dispatch"];
