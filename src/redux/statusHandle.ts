/**
 * Status Handler - For handling network responses
 */
import { AxiosError } from "axios";
import { toast } from "react-toastify";
// import { checkToken, setCredentials } from "@/redux";
// import { store } from "@/redux/store";
// import { getToken } from "@/utils/auth";
// import { api } from './api';
export interface ValidationErrors {
  message: string;
}

const statusHandler = (err: AxiosError<ValidationErrors>) => {
  if (err.response) {
    switch (err.response.status) {
      case 401: {
        // const originalRequest = err.config;
        // const state = store.getState();
        // if (state.auth.token && getToken() && originalRequest?.url !== '/v1/auth/check-token') {
        //   store
        //     .dispatch(checkToken())
        //     .unwrap()
        //     .then((res: any) => {
        //       console.log('res123', res);
        //       if (res?.data?.access_token) {
        //         store.dispatch(setCredentials(res?.data?.access_token));
        //       }
        //       if (originalRequest !== undefined) {
        //         originalRequest.headers['Authorization'] = `Bearer ${state.auth.token}`;
        //       }
        //     });
        // }
        break;
      }
      default: {
        if (err.response.data) {
          console.log("err API", err.response);
          const message = err.response?.data?.message;
          toast.error(message, { toastId: message });
        }
        return err.config;
      }
    }
  }
};

export default statusHandler;
