import { createSlice } from "@reduxjs/toolkit";

export interface ISettingState {
  status: "idle" | "loading" | "rejected" | "fulfilled";
  openSidebar: boolean;
}

const initialState: ISettingState = {
  status: "idle",
  openSidebar: true,
};

const settingSlice = createSlice({
  name: "setting",
  initialState,
  reducers: {
    setOpenSidebar: (state) => {
      state.openSidebar = !state.openSidebar;
    },
  },
  extraReducers: (builder) => {
    builder;
  },
});
export const { setOpenSidebar } = settingSlice.actions;
export default settingSlice.reducer;
