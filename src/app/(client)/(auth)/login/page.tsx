"use client";
import Button from "@/app/components/buttons/Button";
import Card from "@/app/components/cards/Card";
import Input from "@/app/components/forms/Input";
import { useRouter } from "next/navigation";
import React, { useState } from "react";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const router = useRouter();

  const loginhandle = () => {
    router.push("/dashboard");
  };

  return (
    <main className="flex min-h-screen flex-col items-center justify-center p-24">
      <Card className="flex flex-col gap-3">
        <div className="flex justify-center">
          <h1>Login</h1>
        </div>
        <form className="flex flex-col gap-3">
          <div>
            <Input
              id="email"
              value={email}
              label="Email"
              type="email"
              placeholder="email"
              onChange={(e) => setEmail(e.currentTarget.value)}
            />
          </div>
          <div>
            <Input
              id="password"
              type="password"
              value={password}
              label="Password"
              onChange={(e) => setPassword(e.currentTarget.value)}
            />
          </div>
          <div className="flex">
            <Button onClick={loginhandle} variant="primary" className="w-full">
              Login
            </Button>
          </div>
        </form>
      </Card>
    </main>
  );
}
