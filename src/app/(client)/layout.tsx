"use client";
import { ThemeProvider, useTheme } from "next-themes";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import Button from "../components/buttons/Button";
import dynamic from "next/dynamic";
import Switch from "../components/forms/Switch";

const FaSun = dynamic(async () => (await import("react-icons/fa")).FaSun);
const FaMoon = dynamic(async () => (await import("react-icons/fa")).FaMoon);

type props = {
  children: React.ReactNode;
};

export default function ClientLayout({ children }: props) {
  const { theme, setTheme, resolvedTheme } = useTheme();
  const [isDark, setIsDark] = useState(false);

  useEffect(() => {
    resolvedTheme === "light" ? setIsDark(false) : setIsDark(true);
    return () => {};
  }, [resolvedTheme]);

  return (
    <section>
      <nav className="w-full flex items-center h-[6vh] bg-primary text-white justify-between px-6 text-sm">
        <div className="uppercase">
          <h1>
            <Link href={"/"}>Brand</Link>
          </h1>
        </div>
        <ul className="flex gap-4 capitalize items-center">
          <li>
            <Link href={"/"}>home</Link>
          </li>
          <li>
            <Link href={"/contact"}>contact</Link>
          </li>
          <li>
            <Link href={"/login"}>
              <Button variant="dark">Login</Button>
            </Link>
          </li>
          <li className="flex items-center">
            <Switch
              value={isDark ? true : false}
              id="theme-2"
              size="lg"
              onChange={() => setTheme(isDark ? "light" : "dark")}
              icon={
                isDark ? (
                  <span className="text-white">
                    <FaMoon />
                  </span>
                ) : (
                  <span className="text-yellow-500">
                    <FaSun />
                  </span>
                )
              }
            />
          </li>
        </ul>
      </nav>
      {children}
    </section>
  );
}
