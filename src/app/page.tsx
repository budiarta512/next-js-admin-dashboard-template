import ClientLayout from "./(client)/layout";

export default function Home() {
  return (
    
      <ClientLayout>
        <main className="flex min-h-screen flex-col items-center justify-center p-24">
          <h1 className="text-xl text-blue-500">Home</h1>
        </main>
      </ClientLayout>
  );
}
