"use client";
import { ThemeProvider } from "next-themes";
import React from "react";

type props = {
  children: React.ReactNode;
};
export default function ThemeClient({ children }: props) {
  return (
    <ThemeProvider attribute="class" enableSystem>
      {children}
    </ThemeProvider>
  );
}
