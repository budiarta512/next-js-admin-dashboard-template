import React from "react";
import { twMerge } from "tailwind-merge";

type props = {
  children: JSX.Element | JSX.Element[] | string | string[];
  onClick?: (e: React.FormEvent<HTMLButtonElement>) => void;
  variant?: "dark" | "primary" | "blue";
  size?:
    | "sm"
    | "md"
    | "lg"
    | "xl"
    | "square-sm"
    | "square-md"
    | "square-lg"
    | "square-xl";
  className?: string;
  type?: React.ComponentPropsWithoutRef<"button">["type"];
} & React.ComponentPropsWithoutRef<"button">;

export default function Button(props: props) {
  const {
    children,
    onClick,
    variant = "blue",
    size = "md",
    className,
    type = "button",
  } = props;
  const styles = {
    base: `rounded-md`,
    variant: {
      blue: `bg-blue-500 hover:bg-blue-500/80 text-white`,
      primary: `bg-primary hover:bg-primary/80 text-white`,
      secondary: `bg-secondary hover:bg-secondary/80 text-white`,
      dark: `bg-secondary-dark hover:bg-secondary-dark/80 text-white`,
    },
    size: {
      sm: `py-1 px-4 text-sm`,
      md: `py-1.5 px-5 text-sm`,
      lg: `py-2 px-6 text-base`,
      xl: `py-2.5 px-8 text-base`,
      "square-sm": `p-1 text-xs`,
      "square-md": `p-2 text-xs`,
      "square-lg": `p-3 text-sm`,
      "square-xl": `p-4 text-base`,
    },
  };
  return (
    <button
      className={twMerge(
        styles.base,
        styles.size[size],
        styles.variant[variant],
        className
      )}
      type={type}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
