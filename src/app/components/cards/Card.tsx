import React from "react";
import { twMerge } from "tailwind-merge";

type props = {
  className?: string;
  children?: JSX.Element | JSX.Element[] | string | string[];
};
export default function Card({ className, children }: props) {
  const styles = {
    base: `p-4 bg-secondary-light text-gray-800 dark:bg-secondary-dark dark:text-white rounded-md`,
  };
  return <div className={twMerge(styles.base, className)}>{children}</div>;
}
