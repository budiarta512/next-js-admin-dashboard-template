import { usePathname } from "next/navigation";
import { FaChevronDown } from "react-icons/fa";
import { twMerge } from "tailwind-merge";

type props = {
  name: string;
  to?: string;
  children?: JSX.Element | JSX.Element[];
  icon: JSX.Element | JSX.Element[];
  setOpen?: (param: any) => void;
  open?: number;
  initialOpen?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13;
};

const DropdownMenu = ({
  children,
  name,
  icon,
  setOpen,
  open,
  initialOpen = 1,
  to,
}: props) => {
  const pathname = usePathname();
  const styles = {
    totalSubMenu: {
      1: "h-[3rem]",
      2: "h-[5.5rem]",
      3: "h-[8.5rem]",
      4: "h-[11rem]",
      5: "h-[14rem]",
      6: "h-[16.5rem]",
      7: "h-[18.5rem]",
      8: "h-[22rem]",
      9: "h-[25rem]",
      10: "h-[27.5rem]",
      11: "h-[30rem]",
      12: "h-[33.5rem]",
      13: "h-[34.5rem]",
    },
  };

  let active = false;
  // [0] = /
  // [1] = dashboard
  // [3] = page name
  const splitedPathname =
    pathname.split("/")?.length > 0 ? pathname.split("/")[2] : "";

  if (splitedPathname === to) {
    active = true;
  }

  return (
    <div className="m-1">
      <button
        type="button"
        onClick={() =>
          setOpen
            ? open === initialOpen
              ? setOpen(0)
              : setOpen(initialOpen)
            : ""
        }
        className={`flex rounded-xl items-center hover:bg-secondary-dark font-medium hover:text-secondary ${
          active ? "text-secondary bg-secondary-dark" : "text-white"
        } text-sm cursor-pointer w-full transition-all`}
      >
        <div className="p-3">{icon}</div>
        <div className="flex justify-between w-full items-center mx-2">
          <span>{name}</span>
          <div
            className={`transfom transition-all duration-300 ease-in-out ${
              open === initialOpen ? "rotate-180" : ""
            }`}
          >
            <FaChevronDown />
          </div>
        </div>
      </button>
      <ul
        className={twMerge(
          `w-full pl-10 transition-all duration-300 ease-in-out overflow-hidden ${
            open === initialOpen ? styles.totalSubMenu[initialOpen] : "h-0"
          }`
        )}
      >
        {children}
      </ul>
    </div>
  );
};

export default DropdownMenu;
