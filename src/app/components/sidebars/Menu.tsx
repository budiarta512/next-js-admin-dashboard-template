import Link from "next/link";
import { usePathname } from "next/navigation";

type props = {
  icon?: JSX.Element | JSX.Element[];
  name: string;
  to: string;
};
const Menu = ({ icon, name, to }: props) => {
  const pathname = usePathname();

  let active = false;

  if (pathname === to) {
    active = true;
  }
  return (
    <li
      className={`hover:bg-secondary-dark text-sm font-medium m-1 rounded-xl ${
        active ? "bg-secondary-dark" : ""
      }`}
    >
      <Link
        className={`flex gap-2 items-center hover:text-secondary ${
          active ? "text-secondary" : "text-white"
        }`}
        href={to}
      >
        {icon ? (
          <div className="p-3">{icon}</div>
        ) : (
          <div className="py-5"></div>
        )}
        <span className="">{name}</span>
      </Link>
    </li>
  );
};

export default Menu;
