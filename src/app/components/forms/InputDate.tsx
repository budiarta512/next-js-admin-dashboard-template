import React, { useState } from "react";
import Label from "./Label";
import { twMerge } from "tailwind-merge";
// import "react-datepicker/dist/react-datepicker.min.css";
import ReactDatePicker, {
  CalendarContainerProps,
  ReactDatePickerCustomHeaderProps,
  ReactDatePickerProps,
} from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { CgChevronLeft, CgChevronRight } from "react-icons/cg";
import moment from "moment";
import { FaCalendarAlt } from "react-icons/fa";

type props = {
  disable?: boolean;
  className?: string;
  placeholder?: string;
  value: Date | undefined;
  onChange: (a: any) => void;
  label?: string;
  id?: string;
  size?: "xs" | "sm" | "md" | "lg";
  dateFormat?: "yyyy" | "MM/yyyy";
  isRequired?: boolean;
  startDate?: Date | undefined;
  endDate?: Date | undefined;
  selectRange?: boolean;
} & ReactDatePickerProps;

export default function InputDate({
  disable,
  className,
  value,
  onChange,
  placeholder,
  label,
  id,
  size = "md",
  dateFormat,
  isRequired,
  startDate,
  endDate,
  selectRange,
}: props) {
  const [month, setMonth] = useState<Date | undefined>(new Date());
  const styles = {
    base: `relative appearance-none bg-transparent border rounded-md w-full leading-tight focus:ring-1 focus:ring-primary text-sm dark:border-border-dark border-border-light focus:outline-none ${
      disable ? "bg-gray-200" : ""
    }`,
    sizes: {
      xs: `p-0.5 placeholder:text-xs text-sm`,
      sm: `p-1 placeholder:text-sm text-sm`,
      md: `p-1.5 placeholder:text-base text-base`,
      lg: `p-2 placeholder:text-base text-base`,
    },
  };
  return (
    <div className={`flex w-full flex-col ${label ? "gap-2" : ""}`}>
      <Label id={id} isRequired={isRequired}>
        {label}
      </Label>
      <ReactDatePicker
        id={id}
        name={id}
        wrapperClassName="w-full"
        className={`${twMerge(styles.base, styles.sizes[size], className)}`}
        selected={value}
        placeholderText={placeholder ?? "MM/DD/YYYY"}
        onChange={onChange}
        dateFormat={dateFormat}
        // weekDayClassName={(date) => {
        //   return `bg-secondary-light dark:bg-secondary-dark dark:!text-white !text-gray-800 !rounded-md`;
        // }}
        selectsRange={selectRange}
        startDate={startDate}
        endDate={endDate}
        onCalendarOpen={() => setMonth(value)}
        onMonthChange={(e) => setMonth(e)}
        calendarContainer={CalenderContainer}
        showIcon
        calendarIconClassname="dark:!text-color-light !text-color-dark"
        icon={<FaCalendarAlt />}
        renderCustomHeader={
          dateFormat === "yyyy"
            ? undefined
            : (params) => <CustomHeader {...params} dateFormat={dateFormat} />
        }
        dayClassName={(date) =>
          "dark:text-color-light text-color-dark hover:!bg-primary"
        }
        monthClassName={(date) =>
          "dark:text-color-light text-color-dark hover:!bg-primary"
        }
        renderYearContent={(year) => (
          <div className="dark:text-color-light text-color-dark hover:!bg-primary">
            {year}
          </div>
        )}
        showYearPicker={dateFormat === "yyyy"}
        showMonthYearPicker={dateFormat === "MM/yyyy"}
        dateFormatCalendar={dateFormat ?? "MM/DD/YYYY"}
      />
    </div>
  );
}

const CalenderContainer = ({ children, className }: CalendarContainerProps) => {
  return (
    <div
      className={twMerge(
        className,
        "!bg-secondary-light dark:!bg-secondary-dark"
      )}
    >
      {children}
    </div>
  );
};

interface ICustomHeader extends ReactDatePickerCustomHeaderProps {
  dateFormat?: props["dateFormat"];
}

const CustomHeader = ({
  date,
  decreaseMonth,
  increaseMonth,
  prevMonthButtonDisabled,
  nextMonthButtonDisabled,
  increaseYear,
  decreaseYear,
  prevYearButtonDisabled,
  nextYearButtonDisabled,
  dateFormat,
}: ICustomHeader) => {
  let increaseHandle = increaseMonth;
  let decreaseHandle = decreaseMonth;
  let prevDisable = prevMonthButtonDisabled;
  let nextDisable = nextMonthButtonDisabled;
  let format = "MMMM yyyy";

  switch (dateFormat) {
    case "MM/yyyy":
      increaseHandle = increaseYear;
      decreaseHandle = decreaseYear;
      prevDisable = prevYearButtonDisabled;
      nextDisable = nextYearButtonDisabled;
      format = "yyyy";
      break;
    default:
      increaseHandle = increaseMonth;
      decreaseHandle = decreaseMonth;
      prevDisable = prevMonthButtonDisabled;
      nextDisable = nextMonthButtonDisabled;
      format = "MMMM yyyy";
  }
  return (
    <div className="flex items-center justify-between px-2 py-2 bg-secondary-light dark:bg-secondary-dark">
      <div className="flex justify-between w-full">
        <button
          onClick={decreaseHandle}
          disabled={prevDisable}
          type="button"
          className={`
                    ${prevDisable && "cursor-not-allowed opacity-50"}
                    inline-flex p-1 text-sm font-medium text-gray-700 rounded hover:bg-primary-light/80 dark:hover:bg-primary-dark/80 focus:outline-none focus:ring-2 focus:ring-primary
                `}
        >
          <CgChevronLeft className="w-5 h-5 dark:text-color-light text-color-dark" />
        </button>
        <span className="text-lg !text-color-dark dark:!text-color-light">
          {moment(date).format(format)}
        </span>
        <button
          onClick={increaseHandle}
          disabled={nextDisable}
          type="button"
          className={`
                    ${nextDisable && "cursor-not-allowed opacity-50"}
                    inline-flex p-1 text-sm font-medium text-gray-700 rounded hover:bg-primary-light/80 dark:hover:bg-primary-dark/80 focus:outline-none focus:ring-2 focus:ring-primary
                `}
        >
          <CgChevronRight className="w-5 h-5 dark:text-color-light text-color-dark" />
        </button>
      </div>
    </div>
  );
};
