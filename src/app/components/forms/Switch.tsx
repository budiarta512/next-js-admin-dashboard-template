import React from "react";
import Label from "./Label";
import { twMerge } from "tailwind-merge";

type props = {
  value: boolean;
  onChange?: (p: any) => void;
  id?: string;
  label?: string;
  loading?: boolean;
  icon?: JSX.Element | JSX.Element[] | string | string[];
  size?: "sm" | "md" | "lg";
  className?: string;
  classNameSwitch?: string;
};

export default function Switch({
  value,
  id,
  label,
  loading,
  onChange,
  icon,
  size = "md",
  className,
  classNameSwitch,
}: props) {
  //   const toggleClass = "";
  const styles = {
    base: `flex items-center rounded-full p-1 cursor-pointer ${
      value
        ? "bg-blue-500/20 ring-1 ring-blue-500"
        : "bg-gray-100/20 ring-1 ring-gray-400"
    }`,
    toggleClass: {
      lg: "translate-x-7",
      md: "translate-x-7",
      sm: "translate-x-5",
    },
    baseSwitch: `flex justify-center items-center rounded-full shadow-md transform duration-300 ease-in-out
    ${value ? "bg-primary" : "bg-gray-200"}`,
    size: {
      lg: "md:w-16 md:h-8 w-14 h-7",
      md: "md:w-14 md:h-6 w-12 h-6",
      sm: "md:w-13 md:h-5 w-10 h-5",
    },
    sizeSwitch: {
      lg: "md:w-7 md:h-7 h-6 w-6",
      md: "md:w-5 md:h-5 h-4 w-4",
      sm: "md:w-3 md:h-3 h-3 w-3",
    },
  };
  return (
    <div>
      <div className="flex items-center gap-4">
        {/*   Switch Container */}
        {label ? <Label id={id}>{label}</Label> : ""}
        {loading ? (
          <span>...</span>
        ) : (
          <div
            id={id}
            className={twMerge(styles.base, styles.size[size], className)}
            onClick={() => {
              onChange ? onChange(!value) : "";
            }}
          >
            {/* Switch */}
            <div
              className={twMerge(
                styles.baseSwitch,
                styles.sizeSwitch[size],
                `${value ? styles.toggleClass[size] : null}`,
                classNameSwitch
              )}
            >
              {icon}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
