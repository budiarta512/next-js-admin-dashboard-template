import React from "react";
import { twMerge } from "tailwind-merge";
import Label from "./Label";

type props = {
  label?: string;
  value: string;
  onChange: (e: React.FormEvent<HTMLInputElement>) => void;
  id?: string;
  className?: string;
  isRequired?: boolean;
  size?: "sm" | "md" | "lg" | "xl";
  type?: React.InputHTMLAttributes<HTMLInputElement["type"]>;
} & React.ComponentPropsWithoutRef<"input">;

export default function Input(props: props) {
  const {
    value,
    onChange,
    id,
    className,
    label,
    type = "text",
    isRequired,
    size = "md",
    ...rest
  } = props;
  const styles = {
    base: `rounded-md border border-gray-400 outline-none focus:ring-[1.5px] focus:ring-primary/70 bg-transparent`,
    size: {
      sm: "p-0.5 placeholder:text-xs  text-sm",
      md: "p-1 placeholder:text-sm  text-sm",
      lg: "p-1.5 placeholder:text-base  text-base",
      xl: "p-2 placeholder:text-base  text-base",
    },
  };
  return (
    <div className="flex flex-col gap-1">
      {label && <Label isRequired={isRequired}>{label}</Label>}
      <input
        {...rest}
        id={id}
        type={type}
        className={twMerge(styles.base, styles.size[size], className)}
        value={value}
        onChange={onChange}
      />
    </div>
  );
}
