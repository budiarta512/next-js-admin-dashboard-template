import React from "react";

type props = {
  id?: string;
  children?: JSX.Element | JSX.Element[] | string | string[];
  isRequired?: boolean;
};
export default function Label(props: props) {
  const { id, children, isRequired } = props;
  return (
    <label
      className={`relative text-sm font-medium ${!children ? "hidden" : ""} ${
        isRequired ? "after:content-['*'] after:text-red-500 after:ps-1" : ""
      }`}
      htmlFor={id}
    >
      {children}
    </label>
  );
}
