import Button from "@/app/components/buttons/Button";
import Switch from "@/app/components/forms/Switch";
import { useAppSelector } from "@/redux/hooks";
import { useTheme } from "next-themes";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { FaMoon, FaSun } from "react-icons/fa";
import { PiBellFill } from "react-icons/pi";
import { twMerge } from "tailwind-merge";

type props = {
  className?: string;
};
export default function AdminNavbar({ className }: props) {
  const { setTheme, resolvedTheme } = useTheme();
  const [isDark, setIsDark] = useState(false);
  const openSidebar = useAppSelector(
    (state) => state.setting.setting.openSidebar
  );

  useEffect(() => {
    resolvedTheme === "light" ? setIsDark(false) : setIsDark(true);
    return () => {};
  }, [resolvedTheme]);

  const styles = {
    base: `w-full flex items-center h-[8vh] bg-secondary-dark text-white justify-between px-6 text-sm transition-all duration-150 ${
      openSidebar ? "w-[calc(100%_-_14rem)]" : "w-[100%]"
    }`,
  };
  return (
    <section className="w-full flex justify-end">
      <nav className={twMerge(styles.base, className)}>
        <div className="uppercase">
          <h1
            className={`transform transition-all duration-150  ${
              openSidebar ? "ms-0" : "ms-12"
            }`}
          >
            <Link href={"/"}>Brand</Link>
          </h1>
        </div>
        <ul className="flex gap-4 capitalize items-center">
          <li>
            <Link href={"/dashboard"}>home</Link>
          </li>
          <li>
            <Link href={"/"}>Client Page</Link>
          </li>
          <li>
            <Button size="square-md" variant="blue" className="rounded-full">
              <PiBellFill />
            </Button>
          </li>
          <li className="flex items-center">
            <Switch
              value={isDark ? true : false}
              id="theme"
              size="lg"
              onChange={() => setTheme(isDark ? "light" : "dark")}
              icon={
                isDark ? (
                  <span className="text-white">
                    <FaMoon />
                  </span>
                ) : (
                  <span className="text-yellow-500">
                    <FaSun />
                  </span>
                )
              }
            />
          </li>
        </ul>
      </nav>
    </section>
  );
}
