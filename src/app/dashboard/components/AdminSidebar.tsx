import DropdownMenu from "@/app/components/sidebars/DropdownMenu";
import Menu from "@/app/components/sidebars/Menu";
import { useAppSelector } from "@/redux/hooks";
import React, { useState } from "react";
import { BsClipboard2DataFill, BsDatabaseFill } from "react-icons/bs";
import { FaHome } from "react-icons/fa";

type props = {
  // openSidebar: boolean;
};
export default function AdminSidebar({}: props) {
  const openSidebar = useAppSelector(
    (state) => state.setting.setting.openSidebar
  );
  const [open, setOpen] = useState(0);
  return (
    <div
      className={`bg-primary transform transition-all duration-150 w-[14rem] overflow-y-auto ${
        openSidebar ? "translate-x-0" : "-translate-x-[14rem]"
      } fixed top-0 left-0 h-screen text-white`}
    >
      <ul className="flex flex-col mt-[8vh]">
        <Menu name={"Dashboard"} icon={<FaHome />} to="/dashboard" />
        <DropdownMenu
          name="Data Master"
          to="master"
          icon={<BsDatabaseFill />}
          open={open}
          initialOpen={2}
          setOpen={setOpen}
        >
          <Menu name="Product" to="/dashboard/master/product" />
          <Menu name="category" to="/dashboard/master/category" />
        </DropdownMenu>
        <DropdownMenu
          name="Transaction"
          to="transaction"
          icon={<BsClipboard2DataFill />}
          open={open}
          initialOpen={3}
          setOpen={setOpen}
        >
          <Menu name="Transaction" to="/dashboard/transaction/transaction" />
        </DropdownMenu>
      </ul>
    </div>
  );
}
