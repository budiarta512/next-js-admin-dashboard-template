"use client";

import { ThemeProvider } from "next-themes";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import AdminSidebar from "./components/AdminSidebar";
import AdminNavbar from "./components/AdminNavbar";
import { setOpenSidebar } from "@/redux";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";

type props = {
  children: React.ReactNode;
};
export default function AdminLayout({ children }: props) {
  const [isLogin] = useState(true);
  const router = useRouter();
  const openSidebar = useAppSelector(
    (state) => state.setting.setting.openSidebar
  );
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (!isLogin) {
      router.push("/login");
    }
  }, [isLogin, router]);

  return !isLogin ? null : (
    <div>
      <AdminNavbar />
      <AdminSidebar />
      <div className="flex justify-end">
        <button
          onClick={() => dispatch(setOpenSidebar())}
          className={`fixed z-50 transform transition-all duration-150 ${
            openSidebar ? "bg-secondary-dark" : "bg-primary-dark"
          } shadow-lg top-3 left-2 rounded-full overflow-hidden w-12 h-12 flex flex-col items-center justify-center`}
        >
          <div
            className={`bg-white w-[40%] h-0.5 transform transition-all duration-150 ${
              openSidebar
                ? "rotate-[135deg] translate-y-[2px]"
                : "-translate-y-1"
            }`}
          ></div>
          <div
            className={`bg-white w-[40%] h-0.5 transform transition-all duration-150 ${
              openSidebar ? "translate-x-4 opacity-0" : ""
            }`}
          ></div>
          <div
            className={`bg-white w-[40%] h-0.5 transform transition-all duration-150 ${
              openSidebar
                ? "rotate-[225deg] -translate-y-[2px]"
                : "translate-y-1"
            }`}
          ></div>
        </button>
        <div
          className={`transition-all duration-150 ${
            openSidebar ? "w-[calc(100%_-_14rem)]" : "w-[100%]"
          }`}
        >
          <div className="p-4">{children}</div>
        </div>
      </div>
    </div>
  );
}
