"use client";
import React, { useState } from "react";
import Card from "../components/cards/Card";
import InputDate from "../components/forms/InputDate";

export default function Dashboard() {
  const [date, setDate] = useState<(Date & string) | undefined>(undefined);
  const [endDate, setEndDate] = useState<(Date & string) | undefined>(
    undefined
  );
  return (
    <div>
      <div className="w-full flex justify-between items-center mb-4">
        <h1 className="">Dashboard</h1>
        <div className="w-64">
          <InputDate
            size="sm"
            dateFormat={"yyyy"}
            value={date}
            startDate={date}
            endDate={endDate}
            selectRange
            onChange={(e) => {
              const [start, end] = e;
              setDate(start);
              setEndDate(end);
            }}
          />
        </div>
      </div>
      <div className="w-full grid grid-cols-6 gap-6">
        {/* row 1 */}
        <Card className="col-span-6"></Card>
        {/* row 2 */}
        <Card className="col-span-6"></Card>
        {/* row 3 */}
        <Card className="col-span-6"></Card>
      </div>
    </div>
  );
}
