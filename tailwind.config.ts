import type { Config } from "tailwindcss";

const config: Config = {
  darkMode: "class",
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        primary: "#81689D",
        secondary: "#FFD0EC",
        "primary-dark": "#1F2544",
        "secondary-dark": "#474F7A",
        "primary-light": "#EEEEEE",
        "secondary-light": "#DDDDDD",
        "color-dark": "#1e293b",
        "color-light": "#e2e8f0",
        "border-light": "#94a3b8",
        "border-dark": "#475569",
      },
    },
  },
  plugins: [],
};
export default config;
